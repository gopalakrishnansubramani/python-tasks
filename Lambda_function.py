x = lambda a,b : a*b
#print(x(2,3))

def myfun(n):
    return lambda c : c*n

mydoub = myfun(2)
#print(mydoub(4))

class Myclass:
    x = 10

p1 = Myclass()

print(p1.x)

class Person:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def fun_name(self):
        print("Hello, my name is " + self.name)

p1 = Person(str(1),2)
print(p1.age)
p1.fun_name()