#import_libraries
from tkinter import Label
from tkinter import Tk
import time
#title_and_size
app_window = Tk()
app_window.title("Gopal's Digital clock")
app_window.geometry("480x100")
app_window.resizable(1,1)

text_font = ("Boulder", 68, "bold")
background = "DarkSlateGrey"
foreground = "DarkOrange"
border_width = 25

label = Label(app_window, font=text_font, bg=background, fg=foreground, bd=border_width)
label.grid(row=0, column=1)

def digital_clock():
    time_live = time.strftime("%H:%M:%S")
    #hour = time_live.split(':')[0]
    
    label.config(text=time_live)
    label.after(200, digital_clock)

digital_clock()
app_window.mainloop()

